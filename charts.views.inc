<?php

// Define charts style for Views.
function charts_views_plugins() {
  return array(
    'module' => 'charts',
    'style'  => array(     // Declare the charts style plugin
      'charts_test' => array(
        'title'           => t('Chart'),
        'theme'           => 'views_charts',
        'help'            => t('Displays the content in several Chart styles.'),
        'handler'         => 'charts_style_plugin_test',
        'uses row plugin' => TRUE,
        'uses fields'     => TRUE,
        'uses options'    => TRUE,
        'type'            => 'normal',
      ),
    )
  );
}

// Implementation of views_plugin_style
class charts_style_plugin_test extends views_plugin_style {
  // Set default options
  function options(&$options) {
    $options['format'] = 'pie2D';
    $options['height'] = 200;
    $options['width']  = 400;
    $options['color']  = ffffff;
    $options['columns'] = array();
    $options['default'] = '';
    $options['info'] = array();
  }

  /**
   * Generate a form for setting options.
   *
   * @param array $form
   * @param array $form_state
   */
  function options_form(&$form, &$form_state) {
    $form['format'] = array(
      '#type'    => 'select',
      '#title'   => t('Chart format'),
      '#options' => array(
        'line2D'  => t('Line 2D'),
        'hbar2D'  => t('Horizontal Bar 2D'),
        'vbar2D'  => t('Vertical Bar 2D'),
        'pie2D'   => t('Pie 2D'),
        'pie3D'   => t('Pie 3D'),
        'venn'    => t('Venn'),
        'scatter' => t('Scatter Plot')
      ),
      '#default_value' => $this->options['format'],
    );
    $form['height'] = array(
      '#type'          => 'textfield',
      '#title'         => t('Chart Height'),
      '#default_value' => $this->options['height'],
    );
    $form['width'] = array(
      '#type'          => 'textfield',
      '#title'         => t('Chart Width'),
      '#default_value' => $this->options['width'],
    );
    $form['color'] = array(
      '#type'          => 'textfield',
      '#title'         => t('Background Color'),
      '#default_value' => $this->options['color'],
      '#description'   => t('In hexadecimal format (RRGGBB). Do not use the # symbol.'),
    );
  }

  // Define and display a test chart
  function render() {
    // Scan all Views data and insert them into a series
    foreach ($this->view->result as $index => $values) {
      foreach ($values as $key => $value) {
        if ($key == 'nid') {
          $dataarray[$index]['#label'] = 'Node '.$value;
        } else {
          $dataarray[$index]['#value'] = $value;
        };
      }
      $data = array($dataarray);
    }

    // Get chart settings from options form
    $chart = array(
      '#type'   => $this->options['format'],
      '#height' => $this->options['height'],
      '#width'  => $this->options['width'],
      '#color'  => $this->options['color'],
    );

    // Use the view title as the chart title, if one exists.
    if ($this->view->get_title() == '') {
      $chart['#title'] = t('Title');
    } else {
      $chart['#title'] = $this->view->get_title();
    };

    // Insert series into the chart array.
    foreach ($data as $series) {
      $chart[] = $series;
    }

    // Print the chart
    return charts_chart($chart);
  }
}
